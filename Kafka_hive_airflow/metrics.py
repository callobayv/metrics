'''
Примечание:
Если таблица обновляется каждый час, то по логике ограничение по времени не нужно, но т.к. в задании указано
что нужно использовать timestamp из kafka, то буду считать только то, что произошло за час до текущей даты-

where  between CURRENT_TIMESTAMP and from_unixtime(unix_timestamp(CURRENT_TIMESTAMP, 'MM/dd/yyyy HH:mm:ss')-1*3600,
'MM/dd/yyyy HH:mm:ss')

Или можно считать то, что произошло от максимальной даты в таблице минус час. Все зависит от требований

Т.к. у меня данные за прошлое, то буду использовать общее ограничение, чтобы тестовые данные проходили.
Также группирую все по локации и добавляю дату подсчета показателей

'''

from os.path import abspath
from pyspark.sql import SparkSession

hive_location = abspath('spark-warehouse')

spark = SparkSession \
    .builder \
    .appName("Hive Connecting") \
    .config("spark.sql.warehouse.dir", hive_location) \
    .enableHiveSupport() \
    .getOrCreate()

def revenue():
    revenue_calc = spark.sql("select CURRENT_TIMESTAMP as date_calc,location,sum(item_price) as revenue_calc \
    from purchases_rt  \
    where  timestamp_ between from_unixtime(unix_timestamp('12/18/2017 11:00:00', 'MM/dd/yyyy HH:mm:ss')) \
    and CURRENT_TIMESTAMP\
    group by location")
    revenue_calc.write.mode("append").saveAsTable("tmp_revenue_calc")
    spark.sql("insert into purchases_agg select * from tmp_revenue_calc")
    spark.sql("drop table tmp_revenue_calc")
    return revenue_calc

def buyers():
    buyers_calc = spark.sql("select CURRENT_TIMESTAMP as date_calc,location,count(distinct customer_id) as buyers_calc \
    from purchases_rt  \
    where  timestamp_ between from_unixtime(unix_timestamp('12/18/2017 11:00:00', 'MM/dd/yyyy HH:mm:ss')) \
    and CURRENT_TIMESTAMP\
    group by location")
    buyers_calc.write.mode("append").saveAsTable("tmp_buyers_calc")
    spark.sql("insert into purchases_agg select * from tmp_buyers_calc")
    spark.sql("drop table tmp_buyers_calc")
    return buyers_calc

def purchases():
    purchases_calc = spark.sql("select CURRENT_TIMESTAMP as date_calc,location,\
    count(distinct sessionId) as purchases_calc \
    from purchases_rt  \
    where  timestamp_ between from_unixtime(unix_timestamp('12/18/2017 11:00:00', 'MM/dd/yyyy HH:mm:ss')) \
    and CURRENT_TIMESTAMP\
    group by location")
    purchases_calc.write.mode("append").saveAsTable("tmp_purchases_calc")
    spark.sql("insert into purchases_agg select * from tmp_purchases_calc")
    spark.sql("drop table tmp_purchases_calc")
    return purchases_calc

def aov():
    aov_calc = spark.sql("select CURRENT_TIMESTAMP as date_calc,location,\
    sum(item_price)/count(distinct sessionId) as aov_calc \
    from purchases_rt  \
    where  timestamp_ between from_unixtime(unix_timestamp('12/18/2017 11:00:00', 'MM/dd/yyyy HH:mm:ss')) \
    and CURRENT_TIMESTAMP\
    group by location")
    aov_calc.write.mode("append").saveAsTable("tmp_aov_calc")
    spark.sql("insert into purchases_agg select * from tmp_aov_calc")
    spark.sql("drop table tmp_aov_calc")
    return aov_calc
def del_purchases_rt():
    spark.sql("delete from purchases_rt")
