from metrics import revenue,buyers,purchases,aov, del_purchases_rt

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago


default_args = {
    'owner': 'Callobayv',                           # Создатель
    'start_date': days_ago(0),                      # Дата начала
    'depends_on_past': False,
}

with DAG(
    'task_dag',
    default_args=default_args,
    schedule_interval='@hourly / *td(hours=1)',      # Период повтора. У нас раз в час
    catchup=False
) as dag:
    revenue_task = PythonOperator(
        task_id = 'revenue_task',
        python_callable = revenue,
        dag = dag
    )
    buyers_task = PythonOperator(
        task_id='buyers_task',
        python_callable=buyers,
        dag=dag
    )
    purchases_task = PythonOperator(
        task_id='purchases_task',
        python_callable=purchases,
        dag=dag
    )
    aov_task = PythonOperator(
        task_id='aov_task',
        python_callable=aov,
        dag=dag
    )
    del_purchases_rt_task = PythonOperator(
        task_id='del_purchases_rt_task',
        python_callable=del_purchases_rt,
        dag=dag
    )
    revenue_task>>buyers_task>>purchases_task>>aov_task>>del_purchases_rt_task
