from os.path import abspath
from pyspark.sql import SparkSession

hive_location = abspath('spark-warehouse')

# Открываем сессию
spark = SparkSession \
    .builder \
    .appName("Hive Connecting") \
    .config("spark.sql.warehouse.dir", hive_location) \
    .enableHiveSupport() \
    .getOrCreate()


 #spark.sql("drop table purchases_rt;")

# Создаем табличку для иморта данных
spark.sql("create table purchases_rt (sessionId int, item_id int, item_price int,customer_id int,\
location string,timestamp_ TIMESTAMP)\
 row format delimited fields terminated by ',';")

# Импорт данных
spark.sql("load data local inpath 'data_task.txt' overwrite into table purchases_rt;")

# Создаем табличку дла агрегата
spark.sql("create table purchases_agg (time_calc timestamp, location string, val_calc int)\
  USING HIVE;")
