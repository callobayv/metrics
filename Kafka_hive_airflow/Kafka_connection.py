
from pyspark.sql import SparkSession
# from pyspark.sql.functions import explode
# from pyspark.sql.functions import split

# Ниже тестовый вариант с документации. Все работает, но подключение к kafka не работает из-за формата
# Делал по туториалу из документации, но все равно ошибка
#
# spark = SparkSession \
#     .builder \
#     .appName("StructuredNetworkWordCount") \
#     .getOrCreate()
#
# lines = spark \
#     .readStream \
#     .format("socket") \
#     .option("host", "localhost") \
#     .option("port", 9999) \
#     .load()
#
# # Split the lines into words
# words = lines.select(
#    explode(
#        split(lines.value, " ")
#    ).alias("word")
# )
#
# # Generate running word count
# wordCounts = words.groupBy("word").count()
#
# query = wordCounts \
#     .writeStream \
#     .outputMode("complete") \
#     .format("console") \
#     .start()
#
# query.awaitTermination()

spark = SparkSession \
    .builder \
    .appName("StructuredNetworkWordCount") \
    .getOrCreate()

df2 = spark.readStream.format("kafka").option("kafka.bootstrap.servers", "10.1.1.1:9092")\
    .option("subscribe", "topic_name")\
    .option("startingOffsets", "earliest") \
    .option("failOnDataLoss", "true") \
    .load()

df2.writeStream\
.outputMode("append") \
.format("parquet")\
.option('path','/user/default/purchases_rt')\
.option("checkpointLocation", "/user/default/checkpoints")\
.start()\
.awaitTermination()
